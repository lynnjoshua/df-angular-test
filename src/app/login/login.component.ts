import {Component, OnInit} from '@angular/core';
import {LoginFormModel} from '../login-form-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formValue: LoginFormModel;

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit(value: LoginFormModel) {
    this.formValue = value;
  }

}
