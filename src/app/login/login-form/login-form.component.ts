import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginFormModel} from '../../login-form-model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  form: FormGroup;
  @Output() result: EventEmitter<LoginFormModel> = new EventEmitter<LoginFormModel>();

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: [null, [
        Validators.email,
        Validators.required
      ]],
      password: [null, [
        Validators.minLength(4),
        Validators.maxLength(12),
        Validators.required
      ]]
    });
  }

  onSubmit() {
    this.result.emit(this.form.value);
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.email.hasError('minLength') ? 'Must be between 4 and 12 characters long' :
        this.email.hasError('maxlength') ? 'Must be between 4 and 12 characters long' :
          '';
  }

}
